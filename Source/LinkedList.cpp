//
//  LinkedList.cpp
//  CommandLineTool
//
//  Created by Dominic Brown on 10/9/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#include "LinkedList.h"

LinkedList::LinkedList()
{
    head = nullptr;
  
}
LinkedList::~LinkedList()
{
    
}
void LinkedList::add (float itemValue)
{
    Node* newNode = new Node;
    
    newNode->value = itemValue;
    newNode->next = nullptr;
    
    Node* temp = head;
    
    if (temp == nullptr) {
        head = newNode;
    }
    else {
        while (temp != nullptr) {
            temp = temp->next;
        }
        temp->next = newNode;
    }

}
float LinkedList::get(int index) // looks ok to me
{
    int count;
    Node* getPointer = head; //starts at beginning
    while (count < index && getPointer != nullptr) //goes through each value until the index is reached or it runs out of values
    {
        getPointer = getPointer->next; //moves pointer along
    }
    return getPointer->value; //returns value at that point
}
int LinkedList::size()
{
    Node* sizePointer = head;
    int count = 0;
    
    while (sizePointer != nullptr) {
        sizePointer = sizePointer->next;
        count++;
    }
    return count;
}

