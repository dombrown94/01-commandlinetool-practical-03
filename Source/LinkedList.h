//
//  LinkedList.h
//  CommandLineTool
//
//  Created by Dominic Brown on 10/9/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__LinkedList__
#define __CommandLineTool__LinkedList__

#include <iostream>

class LinkedList
{
public:
    /** Initialises the head to point to nullptr */
    LinkedList();
    /** deletes all the nodes in the list */
    ~LinkedList();
    /** adds new items to the end of the array */
    void add (float itemValue);
    /** returns the value stored in the Node at a specified point */
    float get (int index);
    /** returns the number of items */
    int size();
    /** gets address of last value*/
    float* getLastAddress();
    void replaceLastAddress(float* node);
    
private:
    struct Node
    {
        float value;
        Node* next;
    };
    Node* head;
};

#endif /* defined(__CommandLineTool__LinkedList__) */
