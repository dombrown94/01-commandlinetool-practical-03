//
//  template.h
//  CommandLineTool
//
//  Created by Dominic Brown on 10/16/14.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef CommandLineTool_template_h
#define CommandLineTool_template_h

template <class Type>
class Number
{
public:
    Type get() const
    {
        std::cout << "yes value: ";
        return value;
    }
    void set(Type newValue)
    {
        value = newValue;
    }
private:
    Type value;
};

#endif
